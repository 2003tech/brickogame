Addons... hurray.

FYI: zip files are git ignored, so please note that extracting an add-on while 
working on the git might accidentally push it to the brickogame gitlab, which 
is a bad idea.

also for the love of god, don't expect that your blockland add-ons will somehow 
work on brickogame. even though both games use the torque engine (and are a 
continuation of the v0002 code, one is official, and one is maybe illegal
accoroding to the v0002 LICENSE file written by badspot all of those
years ago), they're not the same thing internally. so don't whine at us that
blockland glass or rtb 2-4 or jvs doors or whatever else doesn't load due it 
using blockland-only functions or whatever

-grkb/chazpelo
