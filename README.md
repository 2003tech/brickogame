# **Brickogame**
Brickogame is an in-progress port of the code of Blockland v0002 and the then-popular Return to Blockland mod (with BAC's pack?) to a slightly-updated version of Torque3D version 3.10.1, as it is designed to be a Blockland-esque game. There is also progress done for supporting Blockland Retail addons.

## Brickogame Server
The website and APIs for Brickogame

https://gitlab.com/2003tech/brickogameserver

## Disclaimer

**Both the Server and Game are a work in progress, and are in pre-alpha stages. Some functionality is stll missing.**

Compared to previous Gamerappa projects (2003page and squareBracket/cheeseRox), we're using GitLab instead of GitHub.

### "Are you trying to take over Blockland just like what the TBM devs wanted to do back in '05?"

No. Brickogame is not intended to replace Blockland in any way. This project is done for fun. If you haven't bought Blockland and are planning to get Brickogame as it's free, then please don't and instead buy a copy of Blockland. Blockland is made by actual developers, while Brickogame is just some skiddy crap made by some 15 year old.

As someone who's played Blockland on and off since 2014, compared to what other veterans say about Blockland, it is worth it for your money, even if there are a lack of servers (due to a declining amount of players) and even if RobloxTubers keep telling you that its a rip off of some crappy game with micro-transactions thats somehow nostalgic for a lot of people.

## Contributors

* **Gamerappa/Chazpelo**
Lead developer and creator of Brickogame.

* **TheBirdWasHere/Grumpz**
Maintainer of repository, alpha/beta tester.

### Testers

* **cleber299**
